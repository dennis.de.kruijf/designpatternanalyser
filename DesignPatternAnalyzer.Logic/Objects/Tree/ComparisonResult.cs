﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatternAnalyzer.Logic.Objects.Tree
{
    public class ComparisonResult
    {
        public NodeLink NodeLink { get; private set; }
        public List<NodeLink> Matches { get; private set; }

        private string _toString = "";

        /// <summary>
        /// Creates a <c>ComparisonResult</c> for a <c>NodeLink</c> that has one match
        /// </summary>
        /// <param name="nodeLink"></param>
        /// <param name="match"></param>
        public ComparisonResult(NodeLink nodeLink, NodeLink match): this(nodeLink)
        {
            if (match != null)
            {
                Matches.Add(match);
            }
        }

        /// <summary>
        /// Creates a <c>ComparisonResult</c> for a <c>NodeLink</c> that has multiple matches
        /// </summary>
        /// <param name="nodeLink"></param>
        /// <param name="matches"></param>
        public ComparisonResult(NodeLink nodeLink, List<NodeLink> matches): this(nodeLink)
        {
            if (matches?.Count > 0)
            {
                Matches = matches;
            }
        }

        /// <summary>
        /// Creates a <c>ComparisonResult</c> for a <c>NodeLink</c> that has no matches
        /// </summary>
        /// <param name="nodeLink"></param>
        public ComparisonResult(NodeLink nodeLink)
        {
            Matches = new List<NodeLink>();
            NodeLink = nodeLink;
        }

        public override string ToString()
        {
            return ToString(false);
        }

        /// <summary>
        /// Returns a string representation of the <c>NodeLink</c> with it's matching <c>NodeLinks</c>
        /// </summary>
        /// <param name="printParentNodeLinks"></param>
        /// <returns></returns>
        public string ToString(bool printParentNodeLinks)
        {
            _toString = "";

            if (Matches.Count > 0)
            {
                _toString += NodeLink.ToString(printParentNodeLinks) + " MATCHES\n";

                Matches.ForEach(match => {
                    _toString += "\t* " + match.ToString(printParentNodeLinks);
                    ChildMatchesToString(NodeLink, match, 1);
                });
            } else
            {
                _toString += NodeLink.ToString(printParentNodeLinks) + " HAS NO MATCH\n";
            }

            return _toString;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="thisNodeLink"></param>
        /// <param name="thatNodeLink"></param>
        /// <param name="recursions"></param>
        private void ChildMatchesToString(NodeLink thisNodeLink, NodeLink thatNodeLink, int recursions)
        {
            int matchNr = 1;

            (bool Equal, List<(NodeLink ThisChild, NodeLink ThatChild)> Matches) childMatches = thisNodeLink.ChildrenEqual(thatNodeLink);

            if (thisNodeLink.ChildLinks.Count > 0)
            {
                _toString += " (" + childMatches.Matches.Count + "/" + thisNodeLink.ChildLinks.Count + ")\n";

                foreach ((NodeLink ThisChild, NodeLink ThatChild) match in childMatches.Matches)
                {
                    for (int i = 0; i <= recursions; i++)
                    {
                        _toString += "\t";
                    }

                    _toString += matchNr + ": " + match.ThisChild.ToString() + " MATCHES " + match.ThatChild.ToString();

                    ChildMatchesToString(match.ThisChild, match.ThatChild, recursions + 1);

                    matchNr++;
                }
            }
            else
            {
                _toString += " (1/1)\n";
            }
        }
    }
}
