﻿using System;
using System.Collections.Generic;

namespace DesignPatternAnalyzer.Logic.Objects.Tree
{
	/// <summary>
	/// Class <c>Comparator</c> compares two lists of <c>NodeLinks</c> and checks if the second list is contained by the first
	/// </summary>
	public class Comparator
	{
		private List<NodeLink> _patternLinks;
		private List<NodeLink> _codeLinks;

		private Dictionary<string, NodeLink> _patternReferences;
		private Dictionary<string, NodeLink> _codeReferences;

		private ComparisonReport _comparisonReport;

		/// <summary>
		/// Creates a new <c>Comparator</c> that checks if the second list with "code" <c>NodeLinks</c> is contained by the first list of "pattern" <c>CodeLinks</c>
		/// </summary>
		/// <param name="patternLinks"></param>
		/// <param name="codeLinks"></param>
		public Comparator(List<NodeLink> patternLinks, List<NodeLink> codeLinks)
		{
			_patternLinks = patternLinks;
			_codeLinks = codeLinks;
			_comparisonReport = new ComparisonReport();

			_patternReferences = ParseReferences(patternLinks);
			_codeReferences = ParseReferences(codeLinks);
		}

		/// <summary>
		/// Compares two lists of <c>NodeLinks</c> and returns the results in a <c>ComparisonReport</c>
		/// </summary>
		/// <returns></returns>
		public (bool ContainsPattern, ComparisonReport ComparisonReport) ContainsPattern()
        {
			bool containsPattern = true;

			foreach (NodeLink patternLink in _patternLinks)
            {
				bool equals = false;

				foreach (NodeLink codeLink in _codeLinks)
				{
					if (
						patternLink.Equals(codeLink) && patternLink.ChildrenEqual(codeLink, true).Equal 
						&& (!patternLink.ChildNode.TypeName.Equals("reference") || ReferencesEqual(patternLink, codeLink))
					)
                    {
						_comparisonReport.Add(new ComparisonResult(patternLink, codeLink));
						equals = true;
					}
				}

				if (!equals)
                {
					_comparisonReport.Add(new ComparisonResult(patternLink));
					containsPattern = false;
				}
			}

			return (containsPattern, _comparisonReport);
        }

		/// <summary>
		/// Extracts all <c>NodeLinks</c> that contain a child <c>Node</c> that's been referenced by another <c>NodeLink</c>.
		/// </summary>
		/// <param name="nodeLinks"></param>
		/// <returns></returns>
		private Dictionary<string, NodeLink> ParseReferences(List<NodeLink> nodeLinks)
        {
			Dictionary<string, NodeLink> references = new Dictionary<string, NodeLink>();

			nodeLinks.ForEach(thisLink => { 
				if (thisLink.ChildNode != null && thisLink.ChildNode.Name != null && thisLink.ChildNode.TypeName != null && thisLink.ChildNode.TypeName.Equals("reference"))
                {
					NodeLink referenceLink = nodeLinks.Find(thatLink => !thatLink.ChildNode.TypeName.Equals("reference") && thatLink.ChildNode.Name != null && thatLink.ChildNode.Name.Equals(thisLink.ChildNode.Name));

					if (referenceLink != null && !references.ContainsKey(thisLink.ChildNode.Name))
                    {
						references.Add(thisLink.ChildNode.Name, referenceLink);
                    }
                }				
			});

			return references;
		}

		/// <summary>
		/// Compares two <c>NodeLinks</c> by their child <c>Node</c> of type "reference"
		/// </summary>
		/// <param name="patternLink"></param>
		/// <param name="codeLink"></param>
		/// <returns></returns>
		private bool ReferencesEqual(NodeLink patternLink, NodeLink codeLink)
        {
			if (patternLink.ChildNode.Name == null || codeLink.ChildNode.Name == null  || !patternLink.ChildNode.TypeName.Equals("reference") || !codeLink.ChildNode.TypeName.Equals("reference"))
            {
				return false;
            }

			NodeLink patternReference;
			NodeLink codeReference;

			if (!_patternReferences.TryGetValue(patternLink.ChildNode.Name, out patternReference) || !_codeReferences.TryGetValue(codeLink.ChildNode.Name, out codeReference))
            {
				return false;
            }

			return patternReference.Equals(codeReference) && patternReference.ChildrenEqual(codeReference, true).Equal;
        }
	}
}


