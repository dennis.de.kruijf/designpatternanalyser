﻿using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace DesignPatternAnalyzer.Logic.Objects.Tree
{
    /// <summary>
    /// Class <c>Node</c> represents one token of a code base
    /// </summary>
    public class Node
    {
        public string TypeName { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }

        private Node()
        {}

        /// <summary>
        /// Creates a new <c>Node</c> with a type name, name and location
        /// </summary>
        /// <param name="typeName"></param>
        /// <param name="location"></param>
        /// <param name="name"></param>
        public Node(string typeName, string location,  string name): this()
        {
            TypeName = typeName;
            Location = location;
            if (!String.IsNullOrEmpty(name))
            {
                Name = name;
            }
        }
        
        /// <summary>
        /// Creates a new <c>Node</c> with a type name and name
        /// </summary>
        /// <param name="typeName"></param>
        /// <param name="name"></param>
        public Node(string typeName, string name): this()
        {
            if (name == typeName)
            {
                throw new Exception("Node.typeName can not be null");
            }

            TypeName = typeName;
            Name = name;
        }

        /// <summary>
        /// Creates a new <c>Node</c> with a type name
        /// </summary>
        /// <param name="typeName"></param>
        public Node(string typeName) : this()
        {
            TypeName = typeName;
        }

        /// <summary>
        /// Creates a new <c>Node</c> from an <c>XElement</c>
        /// </summary>
        /// <param name="element"></param>
        public Node(XElement element): this()
        {
            if (element.Attribute("name") != null)
            {
                Name = element.Attribute("name").Value;
            }

            TypeName = element.Name.ToString();
        }
        
        /// <summary>
        /// Returns a string representation of the <c>Node</c>
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return TypeName + (!string.IsNullOrEmpty(Name) ? ": " + Name : "");
        }

        public bool Equals(Node node)
        {
            return TypeName != null && node.TypeName != null && TypeName.Equals(node.TypeName);
        }

        public bool Equals(Node node, bool checkName)
        {
            return Equals(node) && (checkName ? Name == node.Name : true);
        }
    }
}
