﻿using System;
using System.Collections.Generic;

namespace DesignPatternAnalyzer.Logic.Objects.Tree
{
	public class NodeLink
	{
		public NodeLink ParentLink { get; set; }
		public List<NodeLink> ChildLinks { get; set; }
		public Node ParentNode { get; set; }
		public Node ChildNode { get; set; }

		public NodeLink() {
			ChildLinks = new List<NodeLink>();
		}

		public NodeLink(Node parentNode, Node childNode) : this()
		{
			ParentNode = parentNode;
			ChildNode = childNode;
		}

		public bool Equals(NodeLink nodeLink)
		{
			return nodeLink.ParentNode.Equals(ParentNode) &&
			       nodeLink.ChildNode.Equals(ChildNode);
		}

		public (bool Equal, List<(NodeLink ThisChild, NodeLink ThatChild)> Matches) ChildrenEqual(NodeLink nodeLink, bool recursive)
        {
			List<(NodeLink, NodeLink)> foundChildLinks = new List<(NodeLink, NodeLink)>();

			foreach (NodeLink thisChildLink in ChildLinks)
			{
				NodeLink foundChildLink = nodeLink.ChildLinks.Find(thatChildLink => 
					thisChildLink.Equals(thatChildLink) && (!recursive || thisChildLink.ChildrenEqual(thatChildLink, true).Equal)
				);
			
				if (foundChildLink != null)
                {
					foundChildLinks.Add((thisChildLink, foundChildLink));
				}
			}

			return (foundChildLinks.Count == ChildLinks.Count, foundChildLinks);
		}

		public (bool Equal, List<(NodeLink ThisChild, NodeLink ThatChild)> Matches) ChildrenEqual(NodeLink nodeLink)
		{
			return ChildrenEqual(nodeLink, false);
		}

		public override string ToString()
        {
			return LinkToString();
        }

		public string ToString(bool printParent)
        {
			return (printParent && ParentLink != null ? ParentLink.ToString() + " -> " : "") + LinkToString();
		}

		public string ToString(bool printParent, bool printChildren)
        {
			string toString = ToString(printParent);

			if (!printChildren)
            {
				return toString;
            }

			ChildLinks.ForEach(link => {
				toString += " " + link.ToString();
			});

			return toString;
		}

		private string LinkToString()
		{
			return "[" + ParentNode.ToString() + " has " + ChildNode.ToString() + "]";
		}
	}
}
