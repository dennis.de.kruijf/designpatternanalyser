﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatternAnalyzer.Logic.Objects.Tree
{
    public class ComparisonReport
    {
        private List<ComparisonResult> _comparisonResults;

        public ComparisonReport()
        {
            _comparisonResults = new List<ComparisonResult>();
        }

        /// <summary>
        /// Adds a <c>ComparisonResult</c> to the <c>ComparisonReport</c>
        /// </summary>
        /// <param name="comparisonResult"></param>
        public void Add(ComparisonResult comparisonResult)
        {
            // Check wether the NodeLink on this ComparisonResult has already been processed on a precious ComparisonResult
            ComparisonResult previousResult = _comparisonResults.Find(result => 
                result.NodeLink.Equals(comparisonResult.NodeLink) && result.NodeLink.ChildrenEqual(comparisonResult.NodeLink).Equal
            );

            // If no previous result has been found for this Nodelink: just add the ComparisonResult to the list
            if (previousResult == null)
            {
                _comparisonResults.Add(comparisonResult);
            } 
            // If however a previous result was found for this NodeLink: don't add this ComparisoResult to the list but check for new matches in the ComparisonResult.Matches
            else if (comparisonResult.Matches?.Count > 0)
            {
                comparisonResult.Matches.ForEach(match => {
                    // Add a new match to the previous result only when this match doesn't exist yet
                    if (previousResult.Matches.Find(previousMatch =>
                            match.Equals(previousMatch) && match.ChildrenEqual(previousMatch).Equal && match.ParentNode.Equals(previousMatch.ParentNode, true)
                        ) == null
                    )
                    {
                        previousResult.Matches.Add(match);
                    }
                });
            }
        }

        /// <summary>
        /// Returns all the ComparisonResults that don't have any matches for the given NodeLink
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ComparisonResult> GetNonMatches()
		{
            return _comparisonResults.Where(x => x.Matches.Count <= 0);
		}
        /// <summary>
        /// Returns all the ComparisonResults
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ComparisonResult> GetResults()
        {
            return _comparisonResults;
        }


        public override string ToString()
        {
            return ToString(false);
        }

        public string ToString(bool printParentResult)
        {
            string toString = "";

            _comparisonResults.ForEach(result => {
                toString += result.ToString(printParentResult) + "\n";
            });

            return toString;
        }
    }
}
