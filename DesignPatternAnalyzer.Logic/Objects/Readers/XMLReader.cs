﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using DesignPatternAnalyzer.Logic.Objects.Tree;

namespace DesignPatternAnalyzer.Logic.Objects.Readers
{
    /// <summary>
    /// Class <c>XMLReader</c> converts an <c>XDocument</c> into two lists of <c>NodeLinks</c>
    /// </summary>
    public static class XMLReader
    {
        public static List<NodeLink> RootLinks = new List<NodeLink>();
        public static List<NodeLink> AllLinks = new List<NodeLink>();

        /// <summary>
        /// Converts an <c>XDocument</c> into two lists of <c>NodeLinks</c>
        /// </summary>
        /// <param name="xml"></param>
        /// <param name="rootElementAttributeName"></param>
        /// <returns></returns>
        public static (List<NodeLink> RootLinks, List<NodeLink> AllLinks) Read(XDocument xml, string rootElementAttributeName)
        {
            if (xml == null)
            {
                Console.WriteLine("No XML provided");
                return (null, null);
            }

            if (string.IsNullOrEmpty(rootElementAttributeName))
            {
                Console.WriteLine("No root element attribute name provided");
                return (null, null);
            }

            XElement rootElement = xml.Element(rootElementAttributeName);

            if (xml == null || xml.Root == null || rootElement == null)
            {
                Console.WriteLine(rootElementAttributeName + " element not found");
                return (null, null);
            }

            ReadNodeLinks(rootElement, null);

            return (RootLinks, AllLinks);
        }

        public static void ReadNodeLinks(XElement element, NodeLink parentLink)
        {
            Node parentNode = parentLink != null ? parentLink.ChildNode : new Node(element);

            foreach (XElement child in element.Elements())
            {
                NodeLink nodeLink = new NodeLink(
                    parentNode,
                    new Node(child)
                );

                if (parentLink != null)
                {
                    nodeLink.ParentLink = parentLink;
                    if (parentLink.ChildLinks.Find(childLink => childLink.Equals(nodeLink)) == null)
                    {
                        parentLink.ChildLinks.Add(nodeLink);
                    }

                } else
                {
                    RootLinks.Add(nodeLink);
                }

                AllLinks.Add(nodeLink);

                ReadNodeLinks(child, nodeLink);
            }
        }
    }
}
