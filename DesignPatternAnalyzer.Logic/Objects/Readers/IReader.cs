﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DesignPatternAnalyzer.Logic.Objects.Tree;


namespace DesignPatternAnalyzer.Logic.Objects.Readers
{
    interface IReader
    {
        public (List<NodeLink> RootLinks, List<NodeLink> AllLinks) Read(string solutionFolder);
    }
}
