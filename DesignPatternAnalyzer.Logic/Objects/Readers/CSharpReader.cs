﻿using System.Linq;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using DesignPatternAnalyzer.Logic.Objects.Tree;
using System.Collections.Generic;
using Microsoft.CodeAnalysis;
using System.IO;
using System;

namespace DesignPatternAnalyzer.Logic.Objects.Readers
{
    public class CSharpReader : IReader
    {
        public static List<NodeLink> RootLinks { get; set; } = new List<NodeLink>();
        public static List<NodeLink> AllLinks { get; set; } = new List<NodeLink>();

        private static Node rootNode;
     
        // Read all the files that the reader can read.
        public (List<NodeLink> RootLinks, List<NodeLink> AllLinks) Read(string solutionFolder)
        {
            // Get all the child files ending with .cs.
            IEnumerable<string> files = Directory.EnumerateFiles(solutionFolder, "*.cs", SearchOption.AllDirectories);

            // Loop through all the .cs files, if there are any.
            if (files.Any())
            {
                foreach (string file in files)
                {
                    string content = System.IO.File.ReadAllText(file);
                    var syntaxTree = CSharpSyntaxTree.ParseText(content, path: file);
                    var nodes = syntaxTree.GetRoot().DescendantNodes().OfType<MemberDeclarationSyntax>();
                    Parse(nodes);
                }
            }

            return (RootLinks, AllLinks);
        }

        // Parse the classes and interfaces.
        private void Parse(IEnumerable<MemberDeclarationSyntax> nodes)
        {
            // If the root node is not initialized, create one.
            if (rootNode == null)
            {
                rootNode = new Node("code-base");
            }

            foreach (var node in nodes)
            {
                // Check if node is a class.
                if (node is ClassDeclarationSyntax classDeclaration)
                {
                    Node childNode = new Node("class",
                        classDeclaration.GetLocation().GetLineSpan().ToString(),
                        classDeclaration.Identifier.ToString());
                    NodeLink nodeLink = addRootNodeLink(rootNode, childNode);

                    if (classDeclaration.BaseList != null)
                    {
                        foreach (var type in classDeclaration.BaseList.Types)
                        {
                            addReference(nodeLink, childNode, node.GetLocation().GetLineSpan().ToString(),
                                type.ToString());
                        }
                    }

                    // Get all properties of the class.
                    var properties = classDeclaration.Members.OfType<FieldDeclarationSyntax>();

                    if (properties != null)
                    {
                        foreach (var property in properties)
                        {
                            var type = property.Declaration.Type.ToString();
                            if (!IsPrimitive(type))
                            {
                                addReference(nodeLink, childNode, property.GetLocation().GetLineSpan().ToString(), type);
                            }
                            else
                            {
                                addProperty(nodeLink, childNode, property.GetLocation().GetLineSpan().ToString(), type);
                            }
                        }
                    }

                    addModifiers(node.Modifiers, nodeLink, childNode);

                    Parse(classDeclaration.Members, nodeLink);
                    continue;
                }

                // Check if node is an interface.
                if (node is InterfaceDeclarationSyntax interfaceDeclaration)
                {
                    Node childNode = new Node("class",
                        interfaceDeclaration.GetLocation().GetLineSpan().ToString(),
                        interfaceDeclaration.Identifier.ToString());
                    NodeLink nodeLink = addRootNodeLink(rootNode, childNode);

                    if (interfaceDeclaration.BaseList != null)
                    {
                        foreach (var type in interfaceDeclaration.BaseList.Types)
                        {
                            addReference(nodeLink, childNode, node.GetLocation().GetLineSpan().ToString(),
                                type.ToString());
                        }
                    }

                    addModifiers(node.Modifiers, nodeLink, childNode);

                    Parse(interfaceDeclaration.Members, nodeLink);
                }
            }
        }

        // Parse within the classes/interfaces.
        private void Parse(IEnumerable<MemberDeclarationSyntax> nodes, NodeLink parentLink)
        {
            Node parentNode = parentLink.ChildNode;

            foreach (var node in nodes)
            {
                // Check if node is a constructor.
                if (node is ConstructorDeclarationSyntax constructor)
                {
                    Node childNode = new Node("constructor", constructor.GetLocation().GetLineSpan().ToString(), "");
                    NodeLink nodeLink = addNodeLink(parentLink, parentNode, childNode);
                    addModifiers(node.Modifiers, nodeLink, childNode);
                    continue;
                }

                // Check if node is a property.
                if (node is FieldDeclarationSyntax field)
                {
                    Node childNode = new Node("property", field.GetLocation().GetLineSpan().ToString(), "");
                    NodeLink nodeLink = addNodeLink(parentLink, parentNode, childNode);
                    addModifiers(node.Modifiers, nodeLink, childNode);
                    continue;
                }

                // Check if node is a method.
                if (node is MethodDeclarationSyntax method)
                {
                    var type = method.ReturnType.ToString();
                    Node childNode = new Node("method", method.GetLocation().GetLineSpan().ToString(), "");
                    NodeLink nodeLink = addNodeLink(parentLink, parentNode, childNode);
                    addModifiers(node.Modifiers, nodeLink, childNode);

                    if (!IsPrimitive(type))
                    {
                        addReference(nodeLink, childNode, method.GetLocation().GetLineSpan().ToString(),
                            type);
                    }
                    else
                    {
                        addProperty(nodeLink, childNode, method.GetLocation().GetLineSpan().ToString(), type);
                    }

                    continue;
                }
            }
        }

        // Add the node link.
        public NodeLink addNodeLink(NodeLink parentLink, Node parentNode, Node childNode)
        {
            NodeLink nodeLink = new NodeLink(parentNode, childNode);
            AllLinks.Add(nodeLink);

            if (parentLink.ChildLinks.Find(childLink => childLink.Equals(nodeLink)) == null)
            {
                parentLink.ChildLinks.Add(nodeLink);
            }

            return nodeLink;
        }

        // Add the the root node link.
        public NodeLink addRootNodeLink(Node parentNode, Node childNode)
        {
            NodeLink nodeLink = new NodeLink(parentNode, childNode);
            AllLinks.Add(nodeLink);
            RootLinks.Add(nodeLink);

            return nodeLink;
        }
        
        // Add the access modifiers to the given type.
        private void addModifiers(SyntaxTokenList modifiers, NodeLink parentLink, Node parentNode)
        {
            foreach (var modifier in modifiers)
            {
                Node childNode = new Node(modifier.ToString(), modifier.GetLocation().GetLineSpan().ToString(), "");
                addNodeLink(parentLink, parentNode, childNode);
            }
        }

        // Add the given type as a reference.
        public void addReference(NodeLink parentNodeLink, Node childNode, string location, string type)
        {
            if (!IsPrimitive(type))
            {
                Node referenceNode = new Node("reference", location, type);
                addNodeLink(parentNodeLink, childNode, referenceNode);
            }
        }

        // Add the given type as a property.
        public void addProperty(NodeLink parentNodeLink, Node childNode, string location, string type)
        {
            Node propertyNode = new Node(type, location, "");
            addNodeLink(parentNodeLink, childNode, propertyNode);
        }
        
        // Check if the given type is not a c# primitive.
        public static bool IsPrimitive(string type)
        {
            return new[] {
                "string",
                "char",
                "object",
                "byte",
                "sbyte",
                "ushort",
                "short",
                "uint",
                "int",
                "ulong",
                "long",
                "float",
                "double",
                "decimal",
                "DateTime",
            }.Contains(type);
        }


    }
}
