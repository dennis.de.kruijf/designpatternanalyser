﻿using DesignPatternAnalyzer.Logic.Objects.Readers;
using DesignPatternAnalyzer.Logic.Objects.Tree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace DesignPatternAnalyzer
{
    class Program
    {
        static void Main(string[] args)
        {
            var configXml = XDocument.Load(@"..\..\Configs\Singleton.xml");
            var DirectoryToParse = @"..\..\MockCodeConfigs\";

            var config = XMLReader.Read(configXml, "code-base").RootLinks;

            Console.WriteLine("RootLinks.count: " + config.Count);

            // @TODO: change the reader to a generic one, to support more readers.
            var codebase_cs = new CSharpReader().Read(DirectoryToParse).AllLinks;

            Comparator queryExecutor = new Comparator(config, codebase_cs);

            (bool ContainsPattern, ComparisonReport ComparisonReport) result = queryExecutor.ContainsPattern();

            Console.WriteLine("RESULT: PATTERN" + ((!result.ContainsPattern ? " NOT" : "") + " FOUND") + ((!result.ContainsPattern ? "" : "!")));
            Console.WriteLine();

            Console.WriteLine(result.ComparisonReport.ToString(true));
            Console.ReadLine();
        }
    }
}
