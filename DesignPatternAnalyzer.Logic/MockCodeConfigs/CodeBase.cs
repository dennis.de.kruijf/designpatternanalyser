﻿
public class Breakiebreakie
{

}

public sealed class Singleton : SingletonInterface, SingletonInterface2
{
	private static Singleton instance = null;
	private static readonly object padlock = new object();

	private Singleton()
	{
	}

	public static Singleton Instance()
	{
		return new Singleton();
	}

	public string returnText()
	{
		return "test";
	}

	SomeClass SomeMethod()
    {
		return new SomeClass();
    }

	SingletonInterface returnInterface()
	{
		return new Singleton();
	}
}

public interface SingletonInterface
{

}

public interface SingletonInterface2 : SingletonInterface
{

}

public class SomeClass
{
	public static bool someProperty;
}