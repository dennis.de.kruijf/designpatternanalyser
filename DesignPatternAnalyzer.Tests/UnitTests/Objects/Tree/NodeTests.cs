using DesignPatternAnalyzer.Logic.Objects.Tree;
using NUnit.Framework;

namespace DesignPatternAnalyzer.Tests.UnitTests.Objects.Tree
{
    public class NodeTests
    {
        [Test]
        public void TestEqualsCheckNameFalseReturnsTrue()
        {
            var node1 = new Node("class", "singleton");
            var node2 = new Node("class", "factory");
            
            Assert.True(node1.Equals(node2));
        }

        [Test]
        public void TestEqualsCheckNameFalseReturnsFalse()
        {
            var node1 = new Node("class", "singleton");
            var node2 = new Node("method", "factory");
            
            Assert.False(node1.Equals(node2));
        }
        
        [Test]
        public void TestEqualsCheckNameTrueReturnsFalse()
        {
            var node1 = new Node("class", "singleton");
            var node2 = new Node("class", "factory");
            
            Assert.False(node1.Equals(node2, true));
        }
        
        [Test]
        public void TestEqualsCheckNameTrueReturnsTrue()
        {
            var node1 = new Node("class", "singleton");
            var node2 = new Node("class", "singleton");
            
            Assert.True(node1.Equals(node2, true));
        }
    }
}