using System;
using System.Xml.Linq;
using DesignPatternAnalyzer.Logic.Objects.Tree;
using NUnit.Framework;

namespace DesignPatternAnalyzer.Tests.UnitTests.Objects.Tree
{
    public class ComparisonResultTests
    {
        [Test]
        public void TestToString()
        {
            var x = new XElement("static", "");
            var z = new XElement("reference", "");
            var z1 = new XAttribute("name", "Singleton");
            var u = new XElement("constructor", "");
            var t = new XElement("sealed", "");
            var s = new XElement("public", "");
            var q = new XElement("class");
            var q1 = new XAttribute("name", "Singleton");
            var p = new XElement("code-base", "");
            
            z.Add(z1);
            q.Add(q1);

            NodeLink nodeLink1 = new NodeLink(new Node(p), new Node(q));
            
            NodeLink nodeLink2 = new NodeLink(nodeLink1.ChildNode, new Node(s));
            nodeLink2.ParentLink = nodeLink1;
            
            NodeLink nodeLink3 = new NodeLink(nodeLink1.ChildNode, new Node(t));
            nodeLink3.ParentLink = nodeLink1;
            
            NodeLink nodeLink4 = new NodeLink(nodeLink1.ChildNode, new Node(u));
            nodeLink4.ParentLink = nodeLink1;
            
            nodeLink1.ChildLinks.Add(nodeLink2);
            nodeLink1.ChildLinks.Add(nodeLink3);
            nodeLink1.ChildLinks.Add(nodeLink4);
            
            var result = new ComparisonResult(nodeLink1);
            Assert.AreEqual("[code-base has class: Singleton] HAS NO MATCH", result.ToString().Trim());
        }
    }
}