using System;
using System.Linq;
using System.Xml.Linq;
using DesignPatternAnalyzer.Logic.Objects.Tree;
using NUnit.Framework;

namespace DesignPatternAnalyzer.Tests.UnitTests.Objects.Tree
{
    public class ComparisonReportTests
    {
        [Test]
        public void TestAdd()
        {
            var z = new XElement("reference", "");
            var z1 = new XAttribute("name", "Singleton");
            var u = new XElement("constructor", "");
            var t = new XElement("sealed", "");
            var s = new XElement("public", "");
            var q = new XElement("class");
            var q1 = new XAttribute("name", "Singleton");
            var p = new XElement("code-base", "");
            
            z.Add(z1);
            q.Add(q1);

            NodeLink nodeLink1 = new NodeLink(new Node(p), new Node(q));
            
            NodeLink nodeLink2 = new NodeLink(nodeLink1.ChildNode, new Node(s));
            nodeLink2.ParentLink = nodeLink1;
            
            NodeLink nodeLink3 = new NodeLink(nodeLink1.ChildNode, new Node(t));
            nodeLink3.ParentLink = nodeLink1;
            
            NodeLink nodeLink4 = new NodeLink(nodeLink1.ChildNode, new Node(u));
            nodeLink4.ParentLink = nodeLink1;
            
            nodeLink1.ChildLinks.Add(nodeLink2);
            nodeLink1.ChildLinks.Add(nodeLink3);
            nodeLink1.ChildLinks.Add(nodeLink4);
            
            var result1 = new ComparisonResult(nodeLink1);
            var result2 = new ComparisonResult(nodeLink2);
            var result3 = new ComparisonResult(nodeLink3);
            var result4 = new ComparisonResult(nodeLink4);
            
            var report = new ComparisonReport();
            
            report.Add(result1);
            report.Add(result2);
            report.Add(result3);
            report.Add(result4);
            
            Assert.AreEqual(4, report.GetNonMatches().Count());
        }
    }
}