using System;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DesignPatternAnalyzer.Logic.Objects.Readers;
using DesignPatternAnalyzer.Logic.Objects.Tree;
using NUnit.Framework;

namespace DesignPatternAnalyzer.Tests.UnitTests.Objects.Tree
{
    public class NodeLinkTests
    {
        [Test]
        public void ChildrenEqualReturnsTrue()
        { 
            var x = new XElement("static", "");
            var z = new XElement("reference", "");
            var z1 = new XAttribute("name", "Singleton");
            var u = new XElement("constructor", "");
            var t = new XElement("sealed", "");
            var s = new XElement("public", "");
            var q = new XElement("class");
            var q1 = new XAttribute("name", "Singleton");
            var p = new XElement("code-base", "");
            
            z.Add(z1);
            q.Add(q1);

            NodeLink nodeLink1 = new NodeLink(new Node(p), new Node(q));
            
            NodeLink nodeLink2 = new NodeLink(nodeLink1.ChildNode, new Node(s));
            nodeLink2.ParentLink = nodeLink1;
            
            NodeLink nodeLink3 = new NodeLink(nodeLink1.ChildNode, new Node(t));
            nodeLink3.ParentLink = nodeLink1;
            
            NodeLink nodeLink4 = new NodeLink(nodeLink1.ChildNode, new Node(u));
            nodeLink4.ParentLink = nodeLink1;
            
            nodeLink1.ChildLinks.Add(nodeLink2);
            nodeLink1.ChildLinks.Add(nodeLink3);
            nodeLink1.ChildLinks.Add(nodeLink4);
            
            
            NodeLink nodeLink101 = new NodeLink(new Node(p), new Node(q));
            
            NodeLink nodeLink102 = new NodeLink(nodeLink101.ChildNode, new Node(s));
            nodeLink102.ParentLink = nodeLink101;
            
            NodeLink nodeLink103 = new NodeLink(nodeLink101.ChildNode, new Node(t));
            nodeLink103.ParentLink = nodeLink101;
            
            NodeLink nodeLink104 = new NodeLink(nodeLink101.ChildNode, new Node(u));
            nodeLink104.ParentLink = nodeLink101;
            
            nodeLink101.ChildLinks.Add(nodeLink102);
            nodeLink101.ChildLinks.Add(nodeLink103);
            nodeLink101.ChildLinks.Add(nodeLink104);

            Assert.True(nodeLink1.ChildrenEqual(nodeLink101).Equal);
        }
        
        [Test]
        public void ChildrenEqualReturnsFalse()
        { 
            var x = new XElement("static", "");
            var z = new XElement("reference", "");
            var z1 = new XAttribute("name", "Singleton");
            var u = new XElement("constructor", "");
            var t = new XElement("sealed", "");
            var s = new XElement("public", "");
            var q = new XElement("class");
            var q1 = new XAttribute("name", "Singleton");
            var p = new XElement("code-base", "");
            
            z.Add(z1);
            q.Add(q1);

            NodeLink nodeLink1 = new NodeLink(new Node(p), new Node(q));
            
            NodeLink nodeLink2 = new NodeLink(nodeLink1.ChildNode, new Node(s));
            nodeLink2.ParentLink = nodeLink1;
            
            NodeLink nodeLink3 = new NodeLink(nodeLink1.ChildNode, new Node(t));
            nodeLink3.ParentLink = nodeLink1;
            
            NodeLink nodeLink4 = new NodeLink(nodeLink1.ChildNode, new Node(u));
            nodeLink4.ParentLink = nodeLink1;
            
            nodeLink1.ChildLinks.Add(nodeLink2);
            nodeLink1.ChildLinks.Add(nodeLink3);
            nodeLink1.ChildLinks.Add(nodeLink4);
            
            
            NodeLink nodeLink101 = new NodeLink(new Node(p), new Node(q));
            
            NodeLink nodeLink102 = new NodeLink(nodeLink101.ChildNode, new Node(s));
            nodeLink102.ParentLink = nodeLink101;
            
            NodeLink nodeLink103 = new NodeLink(nodeLink101.ChildNode, new Node(t));
            nodeLink103.ParentLink = nodeLink101;

            nodeLink101.ChildLinks.Add(nodeLink102);
            nodeLink101.ChildLinks.Add(nodeLink103);

            Assert.False(nodeLink1.ChildrenEqual(nodeLink101).Equal);
        }
    }
}