using System;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DesignPatternAnalyzer.Logic.Objects.Readers;
using DesignPatternAnalyzer.Logic.Objects.Tree;
using NUnit.Framework;

namespace DesignPatternAnalyzer.Tests.UnitTests.Objects
{
    public class ReaderTests
    {
        [Test]
        public void TestReadNodeLinks()
        {
            //Prepare
            var xml = XDocument.Load("../../../TestData/Xmls/DesignPattern.xml");
            XElement rootElement = xml.Element("code-base");
            
            var x = new XElement("static", "");
            var y = new XElement("public", "");
            var z = new XElement("reference", "");
            var z1 = new XAttribute("name", "Singleton");
            var w = new XElement("method");
            var v = new XElement("private", "");
            var u = new XElement("constructor", "");
            var t = new XElement("sealed", "");
            var s = new XElement("public", "");
            var q = new XElement("class");
            var q1 = new XAttribute("name", "Singleton");
            var p = new XElement("code-base", "");
            
            z.Add(z1);
            q.Add(q1);

            NodeLink nodeLink1 = new NodeLink(new Node(p), new Node(q));
            NodeLink nodeLink2 = new NodeLink(nodeLink1.ChildNode, new Node(s));
            NodeLink nodeLink3 = new NodeLink(nodeLink1.ChildNode, new Node(t));
            NodeLink nodeLink4 = new NodeLink(nodeLink1.ChildNode, new Node(u));
            NodeLink nodeLink5 = new NodeLink(nodeLink4.ChildNode, new Node(v));
            NodeLink nodeLink6 = new NodeLink(nodeLink1.ChildNode, new Node(w));
            NodeLink nodeLink7 = new NodeLink(nodeLink6.ChildNode, new Node(x));
            NodeLink nodeLink8 = new NodeLink(nodeLink6.ChildNode, new Node(y));
            NodeLink nodeLink9 = new NodeLink(nodeLink6.ChildNode, new Node(z));

            nodeLink9.ParentLink = nodeLink6;
            nodeLink8.ParentLink = nodeLink6;
            nodeLink7.ParentLink = nodeLink6;
            nodeLink6.ParentLink = nodeLink1;
            nodeLink5.ParentLink = nodeLink4;
            nodeLink4.ParentLink = nodeLink1;
            nodeLink3.ParentLink = nodeLink1;
            nodeLink2.ParentLink = nodeLink1;

            nodeLink1.ChildLinks.Add(nodeLink2);
            nodeLink1.ChildLinks.Add(nodeLink3);
            nodeLink1.ChildLinks.Add(nodeLink4);
            nodeLink1.ChildLinks.Add(nodeLink6);
            nodeLink4.ChildLinks.Add(nodeLink5);
            nodeLink6.ChildLinks.Add(nodeLink7);
            nodeLink6.ChildLinks.Add(nodeLink8);
            nodeLink6.ChildLinks.Add(nodeLink9);

            List<NodeLink> links = new List<NodeLink>(new[]
                {
                    nodeLink1,
                    nodeLink2,
                    nodeLink3,
                    nodeLink4,
                    nodeLink5,
                    nodeLink6,
                    nodeLink7,
                    nodeLink8,
                    nodeLink9,
                    
                    nodeLink1,
                    nodeLink2,
                    nodeLink3,
                    nodeLink4,
                    nodeLink5,
                    nodeLink6,
                    nodeLink7,
                    nodeLink8,
                    nodeLink9,
                });

            //Act
            XMLReader.ReadNodeLinks(rootElement, null);
            
            StringBuilder actualString = new StringBuilder();
            foreach (var nodeLink in XMLReader.AllLinks)
            {
                actualString.AppendLine(nodeLink.ToString());
            }

            Assert.AreEqual("[code-base has class: Singleton]\r\n[class: Singleton has public]\r\n[class: Singleton has sealed]\n[class: Singleton has constructor]\n[constructor has private]\n[class: Singleton has method]\r\n[method has static]\r\n[method has public]\r\n[method has reference: Singleton]\r\n",
                actualString.ToString());
        }
    }
}