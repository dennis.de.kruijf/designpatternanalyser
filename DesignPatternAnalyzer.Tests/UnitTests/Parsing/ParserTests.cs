using System;
using System.Linq;
using System.Xml.Linq;
using DesignPatternAnalyzer.Logic.Objects.Tree;
using DesignPatternAnalyzer.Logic.Objects.Readers;
using NUnit.Framework;

namespace DesignPatternAnalyzer.Tests.Parsing
{
    public class ParserTests
    {
        [Test]
        public void TestIsPrimitiveReturnsTrue()
        {
            String primitive = "double";
            Assert.IsTrue(CSharpReader.IsPrimitive(primitive));
        }
        
        [Test]
        public void TestIsPrimitiveCaseSensitiveReturnsFalse()
        {
            String primitive = "Double";
            Assert.IsFalse(CSharpReader.IsPrimitive(primitive));
        }
        
        [Test]
        public void TestIsPrimitiveRandomTextReturnsFalse()
        {
            String primitive = "testString";
            Assert.IsFalse(CSharpReader.IsPrimitive(primitive));
        }
        
        [Test]
        public void TestAddNodeLink()
        {
            var xmlClassNode = new Node(new XElement("class"));
            var xmlConstructorNode = new Node(new XElement("constructor"));
            var xmlStaticNode = new Node(new XElement("static"));
            var nodelink = new NodeLink(xmlClassNode, xmlConstructorNode);

            var parser = new CSharpReader();
            var createdNodelink = parser.addNodeLink(nodelink, xmlConstructorNode, xmlStaticNode);
            
            Assert.Contains(createdNodelink, CSharpReader.AllLinks);
            Assert.Contains(createdNodelink, nodelink.ChildLinks);
        }
            
        [Test]
        public void TestAddRootNodeLink()
        {
            var xmlClass = new XElement("class");
            var xmlStatic = new XElement("static");

            CSharpReader parser = new CSharpReader();
            NodeLink nodelink = parser.addRootNodeLink(new Node(xmlClass), new Node(xmlStatic));
            
            Assert.Contains(nodelink, CSharpReader.AllLinks);
            Assert.Contains(nodelink, CSharpReader.RootLinks);
        }

        [Test]
        public void TestAddReference()
        {
            var xmlClassNode = new Node(new XElement("class"));
            var xmlConstructorNode = new Node(new XElement("constructor"));
            var xmlStaticNode = new Node(new XElement("static"));
            var nodelink = new NodeLink(xmlClassNode, xmlConstructorNode);
            
            var referenceNode = new Node("reference", "test-location", "test-type");
            var createdNodeLink = new NodeLink(xmlStaticNode, referenceNode);

            var parser = new CSharpReader();
            parser.addReference(nodelink, xmlStaticNode, "test-location", "test-type");
            
            Assert.AreEqual(2, CSharpReader.AllLinks.Count);
            Assert.AreEqual(createdNodeLink.ToString(), CSharpReader.AllLinks.Find(link => link.Equals(createdNodeLink)).ToString());
        }
    }
}