﻿namespace DesignPatternAnalyzerUI
{
	using DesignPatternAnalyzer.Logic.Objects.Tree;
	using DesignPatternAnalyzer.Logic.Objects.Readers;
    using Microsoft.VisualStudio.Shell.Interop;
    using System;
    using System.Windows;
	using System.Windows.Controls;
	using System.Xml.Linq;
    using System.Windows.Media;

    /// <summary>
    /// Interaction logic for AnalyzeWindowControl.
    /// </summary>
    public partial class AnalyzeWindowControl : UserControl
	{
		
		/// <summary>
		/// Initializes a new instance of the <see cref="AnalyzeWindowControl"/> class.
		/// </summary>
		public AnalyzeWindowControl()
		{
			this.InitializeComponent();

		}

		private void btnAnalayze_Click(object sender, RoutedEventArgs e)
		{
			Microsoft.VisualStudio.Shell.ThreadHelper.ThrowIfNotOnUIThread();
			var item = (ComboBoxItem)cbPattern.SelectedItem;


			IVsSolution solution = (IVsSolution)Microsoft.VisualStudio.Shell.Package.GetGlobalService(typeof(IVsSolution));
			solution.GetSolutionInfo(out string solutionDirectory, out string solutionName, out string solutionDirectory2);
			string solutionPath = solutionDirectory;

			string configLocation = System.IO.Directory.GetParent(System.IO.Directory.GetParent(System.IO.Directory.GetParent(Environment.CurrentDirectory).ToString()).ToString()).ToString() + @"\DesignPatternAnalyzer.Logic\Configs\{0}.xml";
			
			XDocument document = XDocument.Load(string.Format(configLocation, item.Name));


			//Call Result
			//Filter query results;

			var config = XMLReader.Read(document, "code-base").AllLinks;
			var codebase_cs = new CSharpReader().Read(solutionPath).AllLinks;

			Comparator queryExecutor = new Comparator(config, codebase_cs);

			(bool ContainsPattern, ComparisonReport ComparisonReport) result = queryExecutor.ContainsPattern();
			
			var comparisonResults = result.ComparisonReport.GetResults();
			if (result.ContainsPattern)
			{
				resultMessage.DataContext = new AnalyzerResultMessage() { message = "Match Gevonden" };
				resultMessage.Foreground = Brushes.Green;
			}
			else
			{
				resultMessage.DataContext = new AnalyzerResultMessage() { message = "Geen Match Gevonden" };
				resultMessage.Foreground = Brushes.IndianRed;
			}

			resultTree.Items.Clear();
			
			foreach (var report in comparisonResults)
			{
				string[] split = report.ToString().Split('*');
				TreeViewItem treeItem = new TreeViewItem();
				treeItem.Header = split[0];
				split[0].Remove(0, 0);
				foreach(string s in split)
				{
					if(split[0] != s)
					{
						treeItem.Items.Add(new TreeViewItem() { Header = s });					
					}
				}			
				resultTree.Items.Add(treeItem);
			}
		
		}

		internal class AnalyzerResultMessage
		{
			public string message { get; set; }
			public string color { get; set; }

		}
	}
}